"use strict"

const themeToggle = document.getElementById('theme-toggle');
themeToggle.addEventListener('click', toggleTheme);

// Задайте значення за замовчуванням, якщо воно ще не встановлено
const currentTheme = localStorage.getItem('theme') || 'original';
applyTheme(currentTheme);

function toggleTheme() {
  const newTheme = (currentTheme === 'original') ? 'purple' : 'original';
  applyTheme(newTheme);
  localStorage.setItem('theme', newTheme);
}

function applyTheme(theme) {
  document.body.classList.toggle('purple-theme', theme === 'purple');
}

const toggle = document.querySelector('.toggle');
const body = document.body;

toggle.addEventListener('click', function() {
  toggle.classList.toggle('active');
  body.classList.toggle('dark');
});